# Ben project for DICE

This is my submission for the Front-End Technical Exercise for DICE. It utilises the Next.js framework, so it can be run with the standard `yarn dev` command.

You can view a demo deployment at https://ben-dice-twtr-project.herokuapp.com/dicefm

In the initial briefing a GraphQL endpoint for the Twitter API was provided, however this is no longer available. Therefore to workaround this I have scaffolded a new GraphQL API server within this project, that acts as a wrapper around the REST API.

Due to this, a `NEXT_PUBLIC_TWITTER_AUTH_TOKEN` environment variable must be declared for the app to work.

I have also tried to follow the design as closely as possible, however I would note the following issue;

1. The font sizes are too small, in a production scenario these should be increased to aid with legibility and accessibility.
