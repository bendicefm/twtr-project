import { UserProfileFragment } from 'graphql/fragments/userProfile.graphql';
import { decode } from 'html-entities';
import { Tweet } from '../../__generated__/__types__';

async function getTwitterApi(endpoint: string) {
  const response = await fetch(`https://api.twitter.com${endpoint}`, {
    headers: {
      Authorization: `Bearer ${process.env.NEXT_PUBLIC_TWITTER_AUTH_TOKEN}`,
    },
  });

  const json = await response.json();

  return json;
}

function mapUserProfile(user: any): UserProfileFragment {
  return {
    id: user.id_str || user.id.toString(),
    name: user.name,
    username: user.username || user.screen_name,
    tweetsCount: user.statuses_count || null,
    followingCount: user.friends_count || null,
    followersCount: user.followers_count || null,
    profileImageUrl: (
      user.profile_image_url_https || user.profile_image_url
    ).replace('_normal', '_bigger'),
    profileBannerUrl: user.profile_banner_url || null,
  };
}

function mapTweetToStat(originalData: any) {
  const date = new Date(originalData.created_at);

  return {
    id: originalData.id,
    createdAtShort: date.toLocaleDateString('en', {
      day: 'numeric',
      month: 'short',
    }),
    createdAtTimestamp: Math.round(date.getTime() / 1000),
    retweetCount: originalData.public_metrics.retweet_count,
  };
}

export async function getUserProfile(username: string) {
  const data = await getTwitterApi(
    `/1.1/users/show.json?screen_name=${encodeURIComponent(username)}`
  );

  return mapUserProfile(data);
}

export async function getUserTweets(id: string): Promise<Tweet[]> {
  const response = await getTwitterApi(
    `/2/users/${id}/tweets?max_results=20&exclude=replies&tweet.fields=created_at&user.fields=profile_image_url&expansions=author_id,referenced_tweets.id.author_id,attachments.media_keys&media.fields=preview_image_url,url,width,height`
  );

  return response.data.map((tweet) => {
    const retweetRef = tweet?.referenced_tweets?.find(
      (ref) => ref.type === 'retweeted'
    );

    const status = retweetRef
      ? response.includes.tweets.find((retweet) => retweet.id === retweetRef.id)
      : tweet;

    const author = response.includes.users.find(
      (user) => user.id === status.author_id
    );

    return {
      id: tweet.id,
      text: decode(status.text),
      createdAtShort: new Date(tweet.created_at).toLocaleDateString('en', {
        day: 'numeric',
        month: 'short',
      }),
      user: { ...mapUserProfile(author), tweets: [], stats: [] },
      media: status?.attachments?.media_keys
        ?.map((mediaKey) => {
          const mediaObject = response.includes.media.find(
            (includedMedia) => includedMedia.media_key === mediaKey
          );

          if (!mediaObject?.url) {
            return null;
          }

          return {
            id: mediaObject.media_key,
            url: mediaObject.url,
            width: mediaObject.width,
            height: mediaObject.height,
          };
        })
        .filter((media) => media),
    };
  });
}

export async function getUserStats(id: string) {
  const response = await getTwitterApi(
    `/2/users/${id}/tweets?max_results=50&exclude=retweets,replies&tweet.fields=created_at,public_metrics`
  );

  return response.data.map(mapTweetToStat);
}
