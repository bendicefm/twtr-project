import { IResolvers } from 'apollo-server-micro';
import { User } from '../../__generated__/__types__';
import { getUserProfile, getUserTweets, getUserStats } from './twitter';

const resolverMap: IResolvers = {
  Query: {
    async user(_parent, _args, _context, _info): Promise<User> {
      const userNode = _info.fieldNodes.find(
        (node) => node.name.value === 'user'
      );

      const selectedFields = userNode.selectionSet.selections.map(
        (selection: any) => selection.name.value
      );

      const user = await getUserProfile(_args.username);

      const [tweets, stats] = await Promise.all([
        selectedFields.includes('tweets') ? getUserTweets(user.id) : null,
        selectedFields.includes('stats') ? getUserStats(user.id) : null,
      ]);

      return {
        ...user,
        tweets,
        stats,
      };
    },
  },
};

export default resolverMap;
