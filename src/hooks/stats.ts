import { TweetStatBasicFragment } from 'graphql/fragments/tweetStatBasic.graphql';
import { useMemo } from 'react';

export function useTransformedUserStats(stats: TweetStatBasicFragment[]) {
  return useMemo(() => {
    const newStatsArray = [...stats];

    const sortedStats = newStatsArray.sort(
      (a, b) => a.createdAtTimestamp - b.createdAtTimestamp
    );

    const totalRetweets = newStatsArray.reduce(
      (acc, curr) => acc + curr.retweetCount,
      0
    );

    return { sortedStats, totalRetweets };
  }, [stats]);
}
