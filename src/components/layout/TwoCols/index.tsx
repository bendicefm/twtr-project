import styled from 'styled-components';
import { mobile, desktop } from 'utils/media';
import theme from 'utils/theme';

const TwoCols = styled.div({
  marginBottom: theme.spacing.md,
  [mobile]: {
    '> div:first-child': {
      marginBottom: theme.spacing.sm,
    },
  },
  [desktop]: {
    display: 'flex',
    '> div:first-child': {
      width: 240,
      flexShrink: 0,
      marginRight: theme.spacing.sm,
    },
    '> div:last-child': {
      flex: 1,
    },
  },
});

export default TwoCols;
