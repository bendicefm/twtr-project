import styled from 'styled-components';
import theme from 'utils/theme';

const ChartCard = styled.div({
  background: theme.colors.card,
  boxShadow: '0px 5px 15px rgba(0, 0, 0, 0.16)',
});

export default ChartCard;
