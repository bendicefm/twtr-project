import styled from 'styled-components';
import theme from 'utils/theme';

export const HeaderEl = styled.header({
  borderBottomStyle: 'solid',
  borderBottomWidth: 1,
  borderBottomColor: theme.colors.border,
  marginBottom: theme.spacing.sm,
});

export const NavEl = styled.nav({
  display: 'flex',
  paddingLeft: theme.spacing.sm,
  paddingRight: theme.spacing.sm,
});

interface NavLinkProps {
  active: boolean;
}

export const NavLink = styled.a<NavLinkProps>((props) => ({
  display: 'flex',
  height: 50,
  alignItems: 'center',
  justifyContent: 'center',
  fontWeight: theme.fontWeight.bold,
  textDecoration: 'none',
  paddingLeft: theme.spacing.sm,
  paddingRight: theme.spacing.sm,
  fontSize: theme.fontSize.sm,
  color: props.active ? theme.colors.blue : theme.colors.text.strong,
  svg: {
    marginRight: 10,
  },
}));
