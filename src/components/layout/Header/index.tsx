import HomeIcon from 'components/icons/Home';
import StatsIcon from 'components/icons/Stats';
import { useRouter } from 'next/dist/client/router';
import Link from 'next/link';
import React from 'react';
import { HeaderEl, NavEl, NavLink } from './styles';

export default function Header() {
  const router = useRouter();

  return (
    <HeaderEl>
      <NavEl>
        <Link href={`/${router.query.user}`} passHref>
          <NavLink active={router.pathname === '/[user]'}>
            <HomeIcon />
            Home
          </NavLink>
        </Link>
        <Link href={`/${router.query.user}/stats`} passHref>
          <NavLink active={router.pathname === '/[user]/stats'}>
            <StatsIcon />
            Stats
          </NavLink>
        </Link>
      </NavEl>
    </HeaderEl>
  );
}
