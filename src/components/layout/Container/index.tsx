import styled from 'styled-components';
import theme from 'utils/theme';

const Container = styled.div({
  margin: '0 auto',
  width: '100%',
  maxWidth: 753 + theme.spacing.sm * 2,
  paddingLeft: theme.spacing.sm,
  paddingRight: theme.spacing.sm,
});

export default Container;
