import styled from 'styled-components';
import theme from 'utils/theme';

export const ChartTooltipWrapper = styled.div({
  background: theme.colors.blue,
  padding: '10px 15px',
  color: '#fff',
  textAlign: 'center',
});

export const ChartTooltipLabel = styled.div({
  opacity: 0.5,
  marginBottom: 5,
});

export const ChartTooltipValue = styled.div({
  fontWeight: theme.fontWeight.bold,
});
