import React from 'react';
import {
  ChartTooltipLabel,
  ChartTooltipValue,
  ChartTooltipWrapper,
} from './styles';

interface ChartPayload {
  payload: {
    createdAtShort: string;
    retweetCount: number;
  };
}

interface ChartTooltipProps {
  payload?: ChartPayload[];
}

export default function ChartTooltip(props: ChartTooltipProps) {
  if (!props.payload?.[0]) {
    return null;
  }

  const { createdAtShort, retweetCount } = props.payload[0].payload;

  return (
    <ChartTooltipWrapper>
      <ChartTooltipLabel>{createdAtShort}</ChartTooltipLabel>
      <ChartTooltipValue>{retweetCount}</ChartTooltipValue>
    </ChartTooltipWrapper>
  );
}
