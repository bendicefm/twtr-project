import ChartTooltip from 'components/ChartTooltip';
import ChartCard from 'components/layout/ChartCard';
import { TweetStatBasicFragment } from 'graphql/fragments/tweetStatBasic.graphql';
import React from 'react';
import { Area, AreaChart, ResponsiveContainer, Tooltip } from 'recharts';
import theme from 'utils/theme';
import { ChartHeading, ChartSubtitle, ChartTitle } from './styles';

interface UserStatsChartProps {
  stats: TweetStatBasicFragment[];
  totalRetweets: number;
}

export default function UserStatsChart({
  stats,
  totalRetweets,
}: UserStatsChartProps) {
  return (
    <ChartCard>
      <ChartHeading>
        <ChartTitle>Retweets for your latest 50 tweets</ChartTitle>
        <ChartSubtitle>
          {totalRetweets} retweet{totalRetweets === 1 ? '' : 's'}
        </ChartSubtitle>
      </ChartHeading>
      <ResponsiveContainer width="100%" height={286}>
        <AreaChart
          data={stats}
          margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
        >
          <Tooltip content={<ChartTooltip />} />
          <Area
            type="linear"
            dataKey="retweetCount"
            stroke={theme.colors.blue}
            strokeWidth={2}
            fillOpacity={0.16}
            fill={theme.colors.blue}
          />
        </AreaChart>
      </ResponsiveContainer>
    </ChartCard>
  );
}
