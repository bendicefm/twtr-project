import styled from 'styled-components';
import theme from 'utils/theme';

export const ChartHeading = styled.div({
  display: 'flex',
  justifyContent: 'space-between',
  flexWrap: 'wrap',
  fontSize: theme.fontSize.lg,
  padding: theme.spacing.sm,
});

export const ChartTitle = styled.div({
  fontWeight: theme.fontWeight.bold,
  padding: theme.spacing.sm,
});

export const ChartSubtitle = styled.div({
  color: theme.colors.text.muted,
  padding: theme.spacing.sm,
});
