import AvatarBubble from 'components/AvatarBubble';
import styled from 'styled-components';
import theme from 'utils/theme';

export const Card = styled.div({
  background: theme.colors.card,
  lineHeight: 1,
});

export const Banner = styled.div({
  height: 0,
  position: 'relative',
  paddingTop: '50%',
  backgroundSize: 'cover',
  backgroundPosition: 'center center',
});

export const Details = styled.div({
  display: 'flex',
  paddingLeft: theme.spacing.sm,
  paddingRight: theme.spacing.sm,
  marginBottom: 10,
});

export const NameRow = styled.div({
  marginTop: theme.spacing.sm,
  marginLeft: 5,
});

export const NameLabel = styled.div({
  fontSize: theme.fontSize.lg,
  fontWeight: theme.fontWeight.bold,
  marginBottom: 10,
});

export const UsernameLabel = styled.div({
  fontSize: theme.fontSize.sm,
  color: theme.colors.text.muted,
});

interface ShiftedAvatarBubbleProps {
  hasBanner: boolean;
}

export const ShiftedAvatarBubble = styled(
  AvatarBubble
)<ShiftedAvatarBubbleProps>((props) => ({
  marginTop: props.hasBanner ? -25 : 15,
}));

export const MetricsRow = styled.div({
  display: 'flex',
  paddingTop: theme.spacing.sm,
  paddingBottom: theme.spacing.sm,
  '> div': {
    paddingLeft: theme.spacing.sm,
    paddingRight: theme.spacing.sm,
    borderRightStyle: 'solid',
    borderRightWidth: 1,
    borderRightColor: theme.colors.border,
    '&:last-child': {
      borderRightWidth: 0,
    },
  },
});

export const MetricLabel = styled.div({
  fontSize: theme.fontSize.xs,
  color: theme.colors.text.muted,
  marginBottom: 10,
});

export const MetricValue = styled.div({
  fontSize: theme.fontSize.md,
  fontWeight: theme.fontWeight.bold,
  color: theme.colors.blue,
});
