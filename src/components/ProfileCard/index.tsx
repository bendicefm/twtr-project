import AvatarBubble from 'components/AvatarBubble';
import { UserProfileFragment } from 'graphql/fragments/userProfile.graphql';
import React from 'react';
import {
  Banner,
  Card,
  Details,
  MetricLabel,
  MetricsRow,
  MetricValue,
  NameLabel,
  NameRow,
  UsernameLabel,
  ShiftedAvatarBubble,
} from './styles';

interface ProfileCardProps {
  user: UserProfileFragment;
}

export default function ProfileCard({ user }: ProfileCardProps) {
  return (
    <Card>
      {user.profileBannerUrl ? (
        <Banner style={{ backgroundImage: `url(${user.profileBannerUrl})` }} />
      ) : null}
      <Details>
        {user.profileImageUrl ? (
          <ShiftedAvatarBubble
            hasBanner={!!user.profileBannerUrl}
            src={user.profileImageUrl}
          />
        ) : null}
        <NameRow>
          <NameLabel>{user.name}</NameLabel>
          <UsernameLabel>@{user.username}</UsernameLabel>
        </NameRow>
      </Details>
      <MetricsRow>
        <div>
          <MetricLabel>Tweets</MetricLabel>
          <MetricValue>{user.tweetsCount}</MetricValue>
        </div>
        <div>
          <MetricLabel>Following</MetricLabel>
          <MetricValue>{user.followingCount}</MetricValue>
        </div>
        <div>
          <MetricLabel>Followers</MetricLabel>
          <MetricValue>{user.followersCount}</MetricValue>
        </div>
      </MetricsRow>
    </Card>
  );
}
