import styled from 'styled-components';
import theme from 'utils/theme';

const AvatarBubble = styled.img({
  width: 50,
  height: 50,
  borderRadius: '100%',
  overflow: 'hidden',
  borderWidth: 2,
  borderStyle: 'solid',
  borderColor: theme.colors.card,
  position: 'relative',
});

export default AvatarBubble;
