import AvatarBubble from 'components/AvatarBubble';
import { TweetBasicFragment } from 'graphql/fragments/tweetBasic.graphql';
import Link from 'next/link';
import React from 'react';
import {
  InnerRow,
  AvatarCol,
  BodyCol,
  Card,
  AuthorRow,
  TweetBody,
  TweetMedia,
} from './styles';

interface TweetProps {
  tweet: TweetBasicFragment;
}

const mentionRegex = /((?<= |^)@[^@ ]+)/g;

const linkifyText = (text: string) => {
  const split = text.split(mentionRegex);

  const result = split.map((part, index) => {
    if (part.startsWith('@')) {
      const username = part.slice(1);

      return (
        <Link key={index} href={`/${username}`}>
          <a>{part}</a>
        </Link>
      );
    } else {
      return <React.Fragment key={index}>{part}</React.Fragment>;
    }
  });

  return result;
};

export default function Tweet({ tweet }: TweetProps) {
  return (
    <Card>
      <InnerRow>
        <AvatarCol>
          {tweet.user.profileImageUrl ? (
            <Link href={`/${tweet.user.username}`}>
              <a>
                <AvatarBubble src={tweet.user.profileImageUrl} />
              </a>
            </Link>
          ) : null}
        </AvatarCol>
        <BodyCol>
          <AuthorRow>
            <Link href={`/${tweet.user.username}`}>
              <a>
                <strong>{tweet.user.name}</strong> @{tweet.user.username}
              </a>
            </Link>{' '}
            · {tweet.createdAtShort}
          </AuthorRow>
          <TweetBody>{linkifyText(tweet.text)}</TweetBody>
          {tweet.media?.length ? (
            <TweetMedia
              src={tweet.media[0].url}
              width={tweet.media[0].width}
              height={tweet.media[0].height}
            />
          ) : null}
        </BodyCol>
      </InnerRow>
    </Card>
  );
}
