import styled from 'styled-components';
import theme from 'utils/theme';

export const Card = styled.div({
  backgroundColor: theme.colors.card,
  borderBottomWidth: 1,
  borderBottomStyle: 'solid',
  borderBottomColor: theme.colors.border,
  padding: theme.spacing.sm,
});

export const InnerRow = styled.div({
  display: 'flex',
});

export const AvatarCol = styled.div({
  width: 50,
  marginRight: 10,
  flexShrink: 0,
});

export const BodyCol = styled.div({
  flex: 1,
});

export const AuthorRow = styled.div({
  marginBottom: 6,
  fontSize: theme.fontSize.sm,
  color: theme.colors.text.muted,
  a: {
    color: 'inherit',
  },
  strong: {
    color: theme.colors.text.strong,
  },
});

export const TweetBody = styled.p({
  fontSize: theme.fontSize.md,
  marginTop: 0,
  marginBottom: 0,
  lineHeight: 1.5,
});

export const TweetMedia = styled.img({
  width: '100%',
  height: 'auto',
  marginTop: theme.spacing.sm,
});
