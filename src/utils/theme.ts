export default {
  colors: {
    bg: '#F8F8F8',
    card: '#ffffff',
    blue: '#3C74FF',
    text: {
      strong: '#000000',
      muted: 'rgba(0, 0, 0, 0.66)',
    },
    border: 'rgba(0, 0, 0, 0.08)',
  },
  spacing: {
    sm: 15,
    md: 30,
  },
  fontSize: {
    xs: 10,
    sm: 12,
    md: 14,
    lg: 16,
  },
  fontWeight: {
    normal: 400,
    bold: 700,
  },
};
