export const mobile = '@media screen and (max-width: 599px)';

export const desktop = '@media screen and (min-width: 600px)';
