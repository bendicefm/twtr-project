import { AppProps } from 'next/app';
import { ApolloProvider } from '@apollo/client';
import { useApollo } from '../lib/apollo';
import NextNprogress from 'nextjs-progressbar';
import { createGlobalStyle } from 'styled-components';
import theme from 'utils/theme';
import Header from 'components/layout/Header';

const GlobalStyle = createGlobalStyle`
  * {
      box-sizing: border-box;
  }

  body {
    margin: 0;
    padding: 0;
    background-color: ${theme.colors.bg};
    color: ${theme.colors.text.strong};
    font-family: 'Roboto', system-ui, sans-serif;
  }

  a {
    text-decoration: none;
    color: ${theme.colors.blue};
  }
`;

export default function App({ Component, pageProps }: AppProps) {
  const apolloClient = useApollo(pageProps.initialApolloState);

  return (
    <>
      <GlobalStyle />
      <ApolloProvider client={apolloClient}>
        <NextNprogress
          color={theme.colors.blue}
          startPosition={0.3}
          stopDelayMs={200}
          height={3}
          showOnShallow={true}
        />
        <Header />
        <main>
          <Component {...pageProps} />
        </main>
      </ApolloProvider>
    </>
  );
}
