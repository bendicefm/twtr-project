import { useUserQuery, UserDocument } from 'graphql/queries/user.graphql';
import { initializeApollo } from 'lib/apollo';
import { NextApiRequest } from 'next';
import { useRouter } from 'next/dist/client/router';
import Container from 'components/layout/Container';
import Head from 'next/head';
import React, { useMemo } from 'react';
import {
  Area,
  AreaChart,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import theme from 'utils/theme';
import ChartCard from 'components/layout/ChartCard';
import ChartTooltip from 'components/ChartTooltip';
import {
  UserStatsDocument,
  useUserStatsQuery,
} from 'graphql/queries/userStats.graphql';
import UserStatsChart from 'components/UserStatsChart';
import { useTransformedUserStats } from 'hooks/stats';

interface SortableByTimestamp {
  createdAtTimestamp: number;
}

const Stats = () => {
  const router = useRouter();

  const { user } = useUserStatsQuery({
    variables: { username: router.query.user.toString() },
  }).data!;

  const { sortedStats, totalRetweets } = useTransformedUserStats(user.stats);

  return (
    <>
      <Head>
        <title>{user.name || user.username} Stats</title>
      </Head>
      <Container>
        <UserStatsChart stats={sortedStats} totalRetweets={totalRetweets} />
      </Container>
    </>
  );
};

export async function getServerSideProps(req: NextApiRequest) {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: UserStatsDocument,
    variables: {
      username: req.query.user,
    },
  });

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
  };
}

export default Stats;
