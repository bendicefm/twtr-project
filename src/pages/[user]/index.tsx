import { useUserQuery, UserDocument } from 'graphql/queries/user.graphql';
import { initializeApollo } from 'lib/apollo';
import { NextApiRequest } from 'next';
import { useRouter } from 'next/dist/client/router';
import Container from 'components/layout/Container';
import TwoCols from 'components/layout/TwoCols';
import ProfileCard from 'components/ProfileCard';
import Tweet from 'components/Tweet';
import Head from 'next/head';

const Index = () => {
  const router = useRouter();

  const { user } = useUserQuery({
    variables: { username: router.query.user.toString() },
  }).data!;

  return (
    <>
      <Head>
        <title>{user.name || user.username} on Twitter</title>
      </Head>
      <Container>
        <TwoCols>
          <div>
            <ProfileCard key={user.id} user={user} />
          </div>
          <div>
            {user.tweets.map((tweet) => (
              <Tweet tweet={tweet} key={tweet.id} />
            ))}
          </div>
        </TwoCols>
      </Container>
    </>
  );
};

export async function getServerSideProps(req: NextApiRequest) {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: UserDocument,
    variables: {
      username: req.query.user,
    },
  });

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
  };
}

export default Index;
